from django.conf.urls import url
from .import views


urlpatterns = [
    url(r'^$', views.index),  
    url(r'^persona/listar/$', views.persona_list),
    url(r'^persona/detalle/(?P<pk>[0-9]+)/$', views.persona_detalle, ''),
    url(r'^persona/eliminar/(?P<pk>[0-9]+)/$', views.persona_eliminar, ''),
    

]

urlpatterns = [
    url(r'^$', views.index),
    url(r'^mascota/listar/$', views.mascotas_list),
    url(r'^mascota/detalle/(?P<pk>[0-9]+)/$', views.mascota_detalle, ''),
    url(r'^mascota/eliminar/(?P<pk>[0-9]+)/$', views.mascota_eliminar, ''),
]