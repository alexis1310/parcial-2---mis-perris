from django import forms
from .models import Persona, Formulario, Mascota

class FormularioForm(forms.ModelForm):
    class meta:
        model=Formulario
        Fields=('nombre')


class Personaform(forms.ModelForm):
    class Meta:
        model=Persona
        Fields=('nombre','rut','fono','fec','email')

class MascotaForm(forms.ModelForm):
    class Meta:
        model=Mascota
        fields=('Nombre','Raza Predominante','Descripcion','Estado')

