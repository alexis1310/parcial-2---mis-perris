from django.contrib import admin
from .models import Formulario, Persona, Mascota

# Register your models here.

admin.site.register(Formulario)
admin.site.register(Persona)

admin.site.register(Mascota)

