#pylint: disable = no-member

from django.shortcuts import render, get_object_or_404
import time
from .models import Formulario , Persona , Mascota , Estado


# Create your views here.
def index(request):
    a = time.strftime("%c")
    return render(request, 'base.html', {'fechaHora':a})
    
def persona_list(request):
    personas = Persona.objects.all()
    return render (request, 'listar_personas.html', {'Persona':personas})

def persona_detalle(request, pk):
    per = get_object_or_404(Persona,pk=pk)
    return render(request, 'detalle_persona.html',{'Persona':per})

def persona_eliminar(request, pk):
    Persona.objects.filter(pk=pk).delete()
    perso = Persona.objects.all()
    return render (request, 'listar_personas.html', {'Persona':perso})

def persona_editar(request, pk):
    persona = get_object_or_404(Persona, pk=pk)
    print(persona)
    return render (request, 'listar_personas.html', {'Persona':persona})


def mascotas_list(request):
    ma = Mascota.objects.all()
    return render(request, "listar_mascotas.html",{"mascota":ma})

def mascota_detalle(request, pk):
    mascota = get_object_or_404(Mascota, pk=pk)
    return render(request, "detalle_mascotas.html",{"mascota":mascota})

def mascota_eliminar(request, pk):
    Mascota.objects.filter(pk=pk).delete()
    mascota = Mascota.objects.all()
    return render (request, 'eliminar_mascota.html', {'Mascota':mascota})


