from django.db import models

# Create your models here.

class Formulario(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre
        
class Estado(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre


class Persona(models.Model):

    nombre = models.CharField(max_length=100)
    rut = models.CharField(max_length=10)
    fono = models.IntegerField()
    fec = models.DateField('Ingrese año de nacimiento')
    email = models.CharField(max_length=50)
    def __str__(self):
        return '{}  {}  {}  {}  {}'.format(self.nombre, self.rut, str(self.fono), str(self.fec), str(self.email))


class Mascota(models.Model):
    nombre = models.CharField(max_length=100)
    razaPredominante = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to='Mascota', verbose_name=u'Imágen')
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {} {} {}'.format(self.nombre, self.razaPredominante, self.descripcion, self.imagen, self.estado)
